package com.example.chart.service;

import com.example.chart.mapper.ChartMapper;
import com.example.chart.vo.ChartDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ChartService {

    @Autowired
    ChartMapper mapper;

    public List<ChartDataVO> getChartDataList(Map<String, Object> param) throws Exception {
        return mapper.getChartDataList(param);
    }
}
