package com.example.chart.vo;

import lombok.Data;

@Data
public class ChartDataVO {
    private String x;
    private double y;
}
