package com.example.chart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test")
public class ViewController {

    @RequestMapping("/chart")
    public String chart() {
        return "views/chart";
    }

}
