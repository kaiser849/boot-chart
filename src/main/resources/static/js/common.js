
var defaultExpandNode =
	function (rc) {
		console.log(this);
		return this.each(function(){
			if(!this.grid || !this.p.treeGrid) {return;}
			var expanded = this.p.treeReader.expanded_field,
			parent = this.p.treeReader.parent_id_field,
			loaded = this.p.treeReader.loaded,
			level = this.p.treeReader.level_field,
			lft = this.p.treeReader.left_field,
			rgt = this.p.treeReader.right_field;

			if(!rc[expanded]) {
				var id = $.jgrid.getAccessor(rc,this.p.localReader.id);
				var rc1 = $("#" + this.p.idPrefix + $.jgrid.jqID(id),this.grid.bDiv)[0];
				var position = this.p._index[id];

				if(typeof rc1 != 'undefined'){
					console.log($(this).jqGrid("isNodeLoaded",this.p.data[position]));
					if( $(this).jqGrid("isNodeLoaded",this.p.data[position]) ) {
						rc[expanded] = true;
						$("div.treeclick",rc1).removeClass(this.p.treeIcons.plus+" tree-plus").addClass(this.p.treeIcons.minus+" tree-minus");
					} else if (!this.grid.hDiv.loading) {
						rc[expanded] = true;
						$("div.treeclick",rc1).removeClass(this.p.treeIcons.plus+" tree-plus").addClass(this.p.treeIcons.minus+" tree-minus");
						this.p.treeANode = rc1.rowIndex;
						this.p.datatype = this.p.treedatatype;
						if(this.p.treeGridModel === 'nested') {
							$(this).jqGrid("setGridParam",{postData:{nodeid:id,n_left:rc[lft],n_right:rc[rgt],n_level:rc[level]}});
						} else {
							$(this).jqGrid("setGridParam",{postData:{nodeid:id,parentid:rc[parent],n_level:rc[level]}} );
						}
						$(this).trigger("reloadGrid");
						rc[loaded] = true;
						if(this.p.treeGridModel === 'nested') {
							$(this).jqGrid("setGridParam",{postData:{nodeid:'',n_left:'',n_right:'',n_level:''}});
						} else {
							$(this).jqGrid("setGridParam",{postData:{nodeid:'',parentid:'',n_level:''}});
						}
					}

				}
			}

		});
	};

	var defaultCollapseNode =
	function (rc) {
		return this.each(function(){

			if(!this.grid || !this.p.treeGrid) {return;}
			var expanded = this.p.treeReader.expanded_field;
			if(rc[expanded]) {
				rc[expanded] = false;
				var id = $.jgrid.getAccessor(rc,this.p.localReader.id);
				var rc1 = $("#" + this.p.idPrefix + $.jgrid.jqID(id),this.grid.bDiv)[0];
				$("div.treeclick",rc1).removeClass(this.p.treeIcons.minus+" tree-minus").addClass(this.p.treeIcons.plus+" tree-plus");

			}
		});
	};


