const chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
        position: 'top'
    },
    scales: {
        x: {
            type: "time",
            time: {
                unit: 'minute',
                stepSize: 30,
                tooltipFormat: "HH:mm",
                displayFormats: {
                    minute: 'HH:mm'
                }
            }
        }
        ,
        y: {
            ticks: {
                min: 0,
                max: 100,
                stepSize: 10
            }
        }
    }
}

const chartDataSet = {
    labels: [],
    datasets: [{
        label: '',
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: [],
    }]
};


const getChartMaxData = function (charDataSet) {
    let maxData = 0;

    for (let data of charDataSet) {
        if (maxData < data.y) {
            maxData = data.y;
        }
    }
    return maxData;
}


const getChartMinData = function (charDataSet) {
    let mintData = 0;

    for (let data of charDataSet) {
        if (mintData > data.y) {
            mintData = data.y;
        }
    }
    return mintData;
}

const getStepSize = function (maxData) {
    let stepSize = 10;
    if (maxData <= 10) {
        stepSize = 1;
    } else if (maxData <= 100) {
        stepSize = 5;
    } else {
        stepSize = Math.round(maxData / 10);
    }

    return (stepSize);
}
